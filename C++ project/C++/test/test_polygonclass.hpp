#ifndef __TEST_POINTCLASS_H
#define __TEST_POINTCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>


#include "PointClass.hpp"

#include "PointClass.hpp"
#include "PolygonClass.hpp"

using namespace PointNamespace;
using namespace testing;
using namespace std;

using namespace SegmentNamespace;
using namespace PolygonNamespace;

namespace PointTesting {

  TEST(TestPointClass, TestPointMethod)
  {
    Point point(1,2);

    try
    {
      EXPECT_EQ(point.x, 1);
      EXPECT_EQ(point.y, 2);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }
}


namespace PolygonTesting {

  TEST(TestPolygonClass, TestComputeIntersection)
  {
    vector<Point> vertices;
    vertices.resize(4);
    Point p1(1,1), p2(4,1), p3(4,4), p4(1,4);
    vertices[0]= p1;
    vertices[1]= p2;
    vertices[2]= p3;
    vertices[3]= p4;

    vector<int> points;
    points.resize(4);
    for(int i=0; i<4 ; i++)
        points[i]=i+1;
    Point start(2,2), end(3,2);
    vector<Point> segment;
    segment.push_back(start);
    segment.push_back(end);

    Polygon polygon(vertices, points, segment);
    vector<PointInter> intersections = polygon.ShowInterctions();
    vector<Point> expectedInter;

    expectedInter.resize(2);
    Point i1(4,2), i2(1,2);
    expectedInter[0]= i1;
    expectedInter[1]= i2;
    int numIntersections = expectedInter.size();
    try
    {
      for (int k=0; k< numIntersections; k++)
      {
      EXPECT_EQ(intersections[k].p, expectedInter[k]);
      }
    }
    catch (const exception& exception)
    {
      FAIL();
    }

  }
  TEST(TestPolygonClass, TestCutPolygon)
  {
      vector<Point> vertices;
      vertices.resize(6);
      Point p0(1.5,1.0), p1(5.6,1.5), p2(5.5,4.8), p3(4.0,6.2), p4(3.2,4.2), p5(1.0,4.0);
      vertices[0]= p0;
      vertices[1]= p1;
      vertices[2]= p2;
      vertices[3]= p3;
      vertices[4]= p4;
      vertices[5]= p5;

      vector<int> points;
      points.resize(6);
      for(int i=0; i<6 ; i++)
          points[i]=i+1;

      Point start(2.0,3.7), end(4.1,5.9);
      vector<Point> segment;
      segment.push_back(start);
      segment.push_back(end);
      Polygon polygon(vertices, points, segment);
      Point i1(4.2043,6.0093), i2(3.7213,5.5033), i3(2.4086,4.1281) , i4(1.1912,2.8527);

      vector<Point> expectedInter;
      expectedInter.resize(4);
      expectedInter[0]= i1;
      expectedInter[1]= i2;
      expectedInter[2]= i3;
      expectedInter[3]= i4;
      int numIntersections = expectedInter.size();
      vector<PointInter> intersections = polygon.ShowInterctions();
      try
      {
        for (int k=0; k< numIntersections; k++)
        {
        EXPECT_EQ(intersections[k].p, expectedInter[k]);
        }
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<vector<Vertex>> Polygons;
      Polygons.resize(3);
      Polygons[0].reserve(10);
      Polygons[0].push_back(Vertex(p0,0));
      Polygons[0].push_back(Vertex(p1,1));
      Polygons[0].push_back(Vertex(p2,2));
      Polygons[0].push_back(Vertex(i1,6));
      Polygons[0].push_back(Vertex(end,7));
      Polygons[0].push_back(Vertex(i2,8));
      Polygons[0].push_back(Vertex(p4,4));
      Polygons[0].push_back(Vertex(i3,9));
      Polygons[0].push_back(Vertex(start,10));
      Polygons[0].push_back(Vertex(i4,11));


      Polygons[1].reserve(4);
      Polygons[1].push_back(Vertex(p3,3));
      Polygons[1].push_back(Vertex(i2,8));
      Polygons[1].push_back(Vertex(end,7));
      Polygons[1].push_back(Vertex(i1,6));

      Polygons[2].reserve(4);
      Polygons[2].push_back(Vertex(p5,5));
      Polygons[2].push_back(Vertex(i4,11));
      Polygons[2].push_back(Vertex(start,10));
      Polygons[2].push_back(Vertex(i3,9));

      vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

      int numPolygons = Polygons.size();

      try{
      for (int i = 0; i < numPolygons; i++ )
      {
          int numPoints = Polygons[i].size();
          for (int k = 0; k< numPoints; k++)
          {
            EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
            EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);


          }
      }
        }
      catch (const exception& exception)
           {
             FAIL();
           }
      vector<Point> expectedNewPoints(12);
      expectedNewPoints[0]= p0;
      expectedNewPoints[1]= p1;
      expectedNewPoints[2]= p2;
      expectedNewPoints[3]= p3;
      expectedNewPoints[4]= p4;
      expectedNewPoints[5]= p5;
      expectedNewPoints[6]= i1;
      expectedNewPoints[7]= end;
      expectedNewPoints[8]= i2;
      expectedNewPoints[9]= i3;
      expectedNewPoints[10]= start;
      expectedNewPoints[11]= i4;


      vector<Vertex> newPoints = polygon.ShowNewPoints();

      int numNewPoints = 12;
      try {
                for (int k= 0; k< numNewPoints; k++)
                {
                    EXPECT_EQ(expectedNewPoints[k], newPoints[k].p);
                }


      }  catch (const exception& exception)
      {
         FAIL();
      }


   }

  TEST(TestPolygonClass, TestPentagon)
  {
      vector<Point> vertices;
      vertices.resize(5);
      Point p0(2.5,1.0), p1(4.0,2.1), p2(3.4,4.2), p3(1.6,4.2), p4(1.0,2.1);
      vertices[0]= p0;
      vertices[1]= p1;
      vertices[2]= p2;
      vertices[3]= p3;
      vertices[4]= p4;

      vector<int> points;
      points.resize(5);
      for(int i=0; i<5 ; i++)
          points[i]=i+1;

      Point start(1.4,2.75), end(3.6,2.2);
      vector<Point> segment;
      segment.push_back(start);
      segment.push_back(end);
      Polygon polygon(vertices, points, segment);

      Point i1(1.2,2.8);

      vector<vector<Vertex>> Polygons;
      Polygons.resize(2);
      Polygons[0].reserve(6);
      Polygons[0].push_back(Vertex(p0,0));
      Polygons[0].push_back(Vertex(p1,1));
      Polygons[0].push_back(Vertex(end,5));
      Polygons[0].push_back(Vertex(start,6));
      Polygons[0].push_back(Vertex(i1,7));
      Polygons[0].push_back(Vertex(p4,4));


      Polygons[1].reserve(6);
      Polygons[1].push_back(Vertex(p2,2));
      Polygons[1].push_back(Vertex(p3,3));
      Polygons[1].push_back(Vertex(i1,7));
      Polygons[1].push_back(Vertex(start,6));
      Polygons[1].push_back(Vertex(end,5));
      Polygons[1].push_back(Vertex(p1,1));

      vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

      int numPolygons = Polygons.size();

      try{
      for (int i = 0; i < numPolygons; i++ )
      {
          int numPoints = Polygons[i].size();
          for (int k = 0; k< numPoints; k++)
          {
              EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
              EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);


          }
      }
        }
      catch (const exception& exception)
           {
             FAIL();
           }
  }
  TEST(TestPolygonClass, TestRectangle)
  {
      vector<Point> vertices;
      vertices.resize(4);
      Point p0(1,1), p1(5,1), p2(5,3.1), p3(1,3.1);
      vertices[0]= p0;
      vertices[1]= p1;
      vertices[2]= p2;
      vertices[3]= p3;

      vector<int> points;
      points.resize(4);
      for(int i=0; i<4 ; i++)
          points[i]=i+1;

      Point start(2,1.2), end(4,3);
      vector<Point> segment;
      segment.push_back(start);
      segment.push_back(end);
      Polygon polygon(vertices, points, segment);

      Point i1(1.7778,1), i2( 4.1111,3.1);

      vector<vector<Vertex>> Polygons;
      Polygons.resize(2);
      Polygons[0].reserve(6);
      Polygons[0].push_back(Vertex(p0,0));
      Polygons[0].push_back(Vertex(i1,4));
      Polygons[0].push_back(Vertex(start,5));
      Polygons[0].push_back(Vertex(end,6));
      Polygons[0].push_back(Vertex(i2,7));
      Polygons[0].push_back(Vertex(p3,3));


      Polygons[1].reserve(6);
      Polygons[1].push_back(Vertex(p1,1));
      Polygons[1].push_back(Vertex(p2,2));
      Polygons[1].push_back(Vertex(i2,7));
      Polygons[1].push_back(Vertex(end,6));
      Polygons[1].push_back(Vertex(start,5));
      Polygons[1].push_back(Vertex(i1,4));

      vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

      int numPolygons = Polygons.size();

      try{
      for (int i = 0; i < numPolygons; i++ )
      {
          int numPoints = Polygons[i].size();
          for (int k = 0; k< numPoints; k++)
          {
              EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
              EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);


          }
      }
        }
      catch (const exception& exception)
           {
             FAIL();
           }
  }
  TEST(TestPolygonClass, TestProject2a)
  {
      vector<Point> vertices;
      vertices.resize(10);
      Point p0(2,-2), p1(0,-1), p2(3,1), p3(0,2), p4(3,2), p5(3,3), p6(-1,3), p7(-3,1), p8(0,0) , p9(-3,-2);
      vertices[0]= p0;
      vertices[1]= p1;
      vertices[2]= p2;
      vertices[3]= p3;
      vertices[4]= p4;
      vertices[5]= p5;
      vertices[6]= p6;
      vertices[7]= p7;
      vertices[8]= p8;
      vertices[9]= p9;


      vector<int> points;
      points.resize(10);
      for(int i=0; i<10 ; i++)
          points[i]=i+1;

      Point start(-4,-4), end(4,4);
      vector<Point> segment;
      segment.push_back(start);
      segment.push_back(end);
      Polygon polygon(vertices, points, segment);

      Point i1(1.5,1.5),i2(2,2), i3(-2,-2);

      vector<vector<Vertex>> Polygons;
      Polygons.resize(4);
      Polygons[0].reserve(6);
      Polygons[0].push_back(Vertex(p0,0));
      Polygons[0].push_back(Vertex(p1,1));
      Polygons[0].push_back(Vertex(p2,2));
      Polygons[0].push_back(Vertex(i1,10));
      Polygons[0].push_back(Vertex(p8,8));
      Polygons[0].push_back(Vertex(i3,11));


      Polygons[1].reserve(7);
      Polygons[1].push_back(Vertex(p3,3));
      Polygons[1].push_back(Vertex(i2,12));
      Polygons[1].push_back(Vertex(p5,5));
      Polygons[1].push_back(Vertex(p6,6));
      Polygons[1].push_back(Vertex(p7,7));
      Polygons[1].push_back(Vertex(p8,8));
      Polygons[1].push_back(Vertex(i1,10));

      Polygons[2].reserve(3);
      Polygons[2].push_back(Vertex(p4,4));
      Polygons[2].push_back(Vertex(p5,5));
      Polygons[2].push_back(Vertex(i2,12));

      Polygons[3].reserve(3);
      Polygons[3].push_back(Vertex(p9,9));
      Polygons[3].push_back(Vertex(i3,11));
      Polygons[3].push_back(Vertex(p8,8));

      vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

      int numPolygons = Polygons.size();

      try{
      for (int i = 0; i < numPolygons; i++ )
      {
          int numPoints = Polygons[i].size();
          for (int k = 0; k< numPoints; k++)
          {
            EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
            EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);

          }
      }
        }
      catch (const exception& exception)
           {
             FAIL();
           }
  }

TEST(TestPolygonClass, TestProject2b)
  {
      vector<Point> vertices;
      vertices.resize(10);
      Point p0(2,-2), p1(0,-1), p2(3,1), p3(0,2), p4(3,2), p5(3,3), p6(-1,3), p7(-3,1), p8(0,0) , p9(-3,-2);
      vertices[0]= p0;
      vertices[1]= p1;
      vertices[2]= p2;
      vertices[3]= p3;
      vertices[4]= p4;
      vertices[5]= p5;
      vertices[6]= p6;
      vertices[7]= p7;
      vertices[8]= p8;
      vertices[9]= p9;


      vector<int> points;
      points.resize(10);
      for(int i=0; i<10 ; i++)
          points[i]=i+1;

      Point start(0,-4), end(0,4);
      vector<Point> segment;
      segment.push_back(start);
      segment.push_back(end);
      Polygon polygon(vertices, points, segment);

      Point i1(0,-2),i2(0,3);

      vector<vector<Vertex>> Polygons;
      Polygons.resize(5);
      Polygons[0].reserve(3);
      Polygons[0].push_back(Vertex(p0,0));
      Polygons[0].push_back(Vertex(p1,1));
      Polygons[0].push_back(Vertex(i1,10));


      Polygons[1].reserve(4);
      Polygons[1].push_back(Vertex(p2,2));
      Polygons[1].push_back(Vertex(p3,3));
      Polygons[1].push_back(Vertex(p8,8));
      Polygons[1].push_back(Vertex(p1,1));

      Polygons[2].reserve(4);
      Polygons[2].push_back(Vertex(p4,4));
      Polygons[2].push_back(Vertex(p5,5));
      Polygons[2].push_back(Vertex(i2,11));
      Polygons[2].push_back(Vertex(p3,3));

      Polygons[3].reserve(5);
      Polygons[3].push_back(Vertex(p6,6));
      Polygons[3].push_back(Vertex(p7,7));
      Polygons[3].push_back(Vertex(p8,8));
      Polygons[3].push_back(Vertex(p3,3));
      Polygons[3].push_back(Vertex(i2,11));

      Polygons[4].reserve(4);
      Polygons[4].push_back(Vertex(p9,9));
      Polygons[4].push_back(Vertex(i1,10));
      Polygons[4].push_back(Vertex(p1,1));
      Polygons[4].push_back(Vertex(p8,8));


      vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

      int numPolygons = Polygons.size();

      try{
      for (int i = 0; i < numPolygons; i++ )
      {
          int numPoints = Polygons[i].size();
          for (int k = 0; k< numPoints; k++)
          {
            EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
            EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);

          }
      }
        }
      catch (const exception& exception)
           {
             FAIL();
           }
  }

TEST(TestPolygonClass, TestParallel)
{
    vector<Point> vertices;
    vertices.resize(6);
    Point p0(1.5,1.0), p1(5.6,1.5), p2(5.5,4.8), p3(4.0,6.2), p4(3.2,4.2), p5(1.0,4.0);
    vertices[0]= p0;
    vertices[1]= p1;
    vertices[2]= p2;
    vertices[3]= p3;
    vertices[4]= p4;
    vertices[5]= p5;


    vector<int> points;
    points.resize(6);
    for(int i=0; i<6 ; i++)
        points[i]=i+1;

    Point start(1.66,4.06), end(4.3,4.3);
    vector<Point> segment;
    segment.push_back(start);
    segment.push_back(end);
    Polygon polygon(vertices, points, segment);

    Point i1(5.5118,4.4102);

    vector<vector<Vertex>> Polygons;
    Polygons.resize(2);
    Polygons[0].reserve(7);
    Polygons[0].push_back(Vertex(p0,0));
    Polygons[0].push_back(Vertex(p1,1));
    Polygons[0].push_back(Vertex(i1,6));
    Polygons[0].push_back(Vertex(end,7));
    Polygons[0].push_back(Vertex(p4,4));
    Polygons[0].push_back(Vertex(start,8));
    Polygons[0].push_back(Vertex(p5,5));


    Polygons[1].reserve(5);
    Polygons[1].push_back(Vertex(p2,2));
    Polygons[1].push_back(Vertex(p3,3));
    Polygons[1].push_back(Vertex(p4,4));
    Polygons[1].push_back(Vertex(end,7));
    Polygons[1].push_back(Vertex(i1,6));


    vector<Polygon> cutPolygons = polygon.ShowNewPolygons();

    int numPolygons = Polygons.size();

    try{
    for (int i = 0; i < numPolygons; i++ )
    {
        int numPoints = Polygons[i].size();
        for (int k = 0; k< numPoints; k++)
        {
          EXPECT_EQ(Polygons[i][k].p, cutPolygons[i].GetVertex(k).p);
          EXPECT_EQ(Polygons[i][k].id, cutPolygons[i].GetVertex(k).id);


        }
    }
      }
    catch (const exception& exception)
         {
           FAIL();
         }
}

}
#endif // __TEST_POINTCLASS_H
