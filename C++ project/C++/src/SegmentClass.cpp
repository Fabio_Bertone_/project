#include "SegmentClass.hpp"

namespace SegmentNamespace {

Segment::Segment(Point& p1, Point& p2)
  {
    start = p1;
    end = p2;
    intersection = false;
    parallel = false;
    pointIntersection = Point();
}

}
