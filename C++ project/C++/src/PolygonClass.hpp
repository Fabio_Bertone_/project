#ifndef POLYGONCLASS_H
#define POLYGONCLASS_H

#include <exception>
#include <vector>
#include <algorithm>
#include "Eigen"
#include "PointClass.hpp"
#include "SegmentClass.hpp"

using namespace std;
using namespace Eigen;
using namespace PointNamespace;
using namespace SegmentNamespace;

namespace PolygonNamespace {

class Polygon{
private:
  vector<Segment> edges;
  vector<Vertex> points;
  vector <bool> reached;
  vector<PointInter> interPoints;
  vector<vector<PointInter>> setTaglio;
  Segment segment;
  vector<Polygon> newPolygons;
  vector<Vertex> newPoints;
  int numEdges;

public:
  Polygon(vector<Point>& Points, vector<int>& Edges, vector<Point>& Segment);
  Polygon() {}
  vector<PointInter> ShowInterctions();
  vector<Polygon> ShowNewPolygons();
  vector<Vertex> ShowNewPoints();
  void AddVertex(Vertex& p);
  void AddInteriorPoint(Vertex& p, int& pos);
  void ChangeSegment(Point& p1, Point& p2);
  void ClearPolygon();
  Vertex GetVertex(int& pos);
  int NumEdges();

private:
  void ComputeIntersection();
  void CreateSet();
  void CutPolygon();
  int FindSetTaglio (Point& p);
  int FindEdge (Point& p);
  void NumSubPolygons();
  int FindVertexId(Point& p);
  void AddSegmentPoints();
  int FindNextSet(Point& p, int& id, Point& StartPolygon);
  bool LeftOfSegment(Point& p);
  bool SameSide(Point& p, Point& q);
  void DuplicatePointSet();
  void ParallelCase();
  int AddSetsPoints(int& id, Polygon& newPolygon, int start, int& i);


};
}

#endif // POLYGONCLASS_H
