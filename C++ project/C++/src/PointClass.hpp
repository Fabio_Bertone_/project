#ifndef POINTCLASS_H
#define POINTCLASS_H

#include <exception>
#include <vector>
#include <algorithm>
#include "Eigen"

using namespace std;
using namespace Eigen;

namespace PointNamespace {

  class Point {
    public:
      double x;
      double y;

      Point(double _x, double _y);
      Point() {}
      bool operator== (const Point& p2) const;
      bool operator!=(const Point &p2) const;
    };

  class PointInter : Point {
    public:
      Point p;
      double t;

    PointInter(Point& q, double parameter);
    PointInter() {}
    bool operator< (const PointInter& p2) const;
    bool operator> (const PointInter& p2) const;

  };

  class Vertex : Point{
    public:
      Point p;
      int id;

    Vertex(Point& q, int name);
    Vertex() {}


  };





}

#endif // POINTCLASS_H
