#include "PointClass.hpp"
#define ERR 10E-4
#include <iostream>


namespace PointNamespace {

  Point::Point(double _x, double _y)
  {
     x = _x;
     y = _y;

  }

  bool Point::operator==(const Point &p2) const
  {
      return (abs(x - p2.x)<ERR and abs(y- p2.y)<ERR);
  }

  bool Point::operator!=(const Point &p2) const
  {
      return (abs(x - p2.x)>ERR or abs(y- p2.y)>ERR);
  }

  PointInter::PointInter(Point& q, double parameter)
  {
      p = q;
      t = parameter;
  }


  bool PointInter::operator<(const PointInter &p2) const
  {
      return t < p2.t;
  }

  bool PointInter::operator>(const PointInter &p2) const
  {
      return t > p2.t;
  }

  Vertex::Vertex(Point& q, int name)
  {
      p = q;
      id = name;
  }

}
