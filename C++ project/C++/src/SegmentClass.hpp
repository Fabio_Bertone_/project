#ifndef SEGMENTCLASS_H
#define SEGMENTCLASS_H

#include <exception>
#include <vector>
#include <algorithm>
#include "Eigen"
#include "PointClass.hpp"

using namespace std;
using namespace Eigen;

using namespace PointNamespace;

namespace SegmentNamespace {

class Segment{
  public:
    Point start;
    Point end;
    bool intersection;
    bool parallel;
    Point pointIntersection;

    Segment(Point& p1, Point& p2);
    Segment() {}
 };
}

#endif // SEGMENTCLASS_H
