#include "PolygonClass.hpp"
#define ERR 10E-4

namespace PolygonNamespace {

Polygon::Polygon(vector<Point>& Points, vector<int>& Edges, vector<Point>& Segment)
{
 int numPoints = Points.size();
 points.resize(numPoints);
 newPoints.resize(numPoints);
 edges.resize(numPoints);
 reached.resize(numPoints);
 numEdges = 0;

  for (int i= 0; i < numPoints; i++)
    {
      points[i].p = Points[i];
      points[i].id = i;
      newPoints[i].p = Points[i];
      newPoints[i].id = i;
      edges[i].start = Points[i];
      edges[i].end = Points[(i+1)%numPoints];
      edges[i].intersection = false;
      edges[i].parallel = false;
      reached[i] = false;
      numEdges++;
    }

  // Orientamento sempre da destra verso sinistra, semplifica poi con l'uso del parametro

  //// Controllare se serve veramente anche "and Segment[0].y < Segment[1].y"
 if (Segment[0].x < Segment[1].x )
 {
     segment.start = Segment[0];
     segment.end = Segment[1];
 }
 else if(Segment[0].x != Segment[1].x)
 {
     segment.start = Segment[1];
     segment.end = Segment[0];
 }
 else
 {
     segment.start = Point(Segment[0].x,min(Segment[0].y, Segment[1].y));
     segment.end =  Point(Segment[0].x,max(Segment[0].y, Segment[1].y));
 }


}

int Polygon::NumEdges()
{
    return numEdges;
}

void Polygon::AddVertex(Vertex& p)
{
   points.push_back(p);
   newPoints.push_back(p);
   reached.push_back(false);
   if (points.size() > 1)
   {
       if (edges.size() !=0)
           edges.pop_back();
       Segment s1 = Segment(points[points.size()-2].p, p.p);
       Segment s2 = Segment(p.p, points[0].p);
       edges.push_back(s1);
       edges.push_back(s2);
       if (numEdges == 0)
           numEdges += 2;
       else
           numEdges++;
   }
}

void Polygon::AddInteriorPoint(Vertex& p, int& pos)
{
    edges.erase(edges.begin() + pos);
    points.insert(points.begin() + pos+1, p);
    edges.insert(edges.begin()+pos, Segment(points[pos].p, p.p));
    edges.insert(edges.begin()+pos+1, Segment(p.p,points[(pos+1)%numEdges].p));
    numEdges++;
}

void Polygon::ClearPolygon()
{
    interPoints.clear();
    setTaglio.clear();
    newPolygons.clear();
    for (int i =0; i<numEdges; i++)
    {
        reached[i] = false;
        edges[i].intersection = false;
        edges[i].parallel = false;
    }
    for (int k = int(points.size()) ; k < int(newPoints.size()); k++)
       {
            newPoints.pop_back();
    }
}

Vertex Polygon::GetVertex(int& pos)
{
    return points[pos];
}


void Polygon::ChangeSegment(Point& p1, Point& p2)
{
    if (p1.x < p2.x)
        segment = Segment(p1,p2);
    else if(p1.x != p2.x)
        segment = Segment(p2,p1);
    else
    {
        Point t1 = Point(p1.x, min(p1.y, p2.y));
        Point t2 = Point(p1.x, min(p1.y, p2.y));
        segment = Segment(t1, t2);
    }
    ClearPolygon();
}



void Polygon::ComputeIntersection()
{
    double x1, y1;

    // direzione segmento
    x1 = segment.end.x - segment.start.x;
    y1 = segment.end.y - segment.start.y;
    Vector2d vector1(x1,y1);

    for (int i= 0; i< numEdges; i++)
    {
        double xi, yi;
        // direzione lato
        xi = edges[i].end.x - edges[i].start.x;
        yi = edges[i].end.y - edges[i].start.y;
        Vector2d vector(-yi, xi);

        //Ruoto vettore e usato dot product, per vedere se perpendicolare a quello ruotato e quindi paralleli
        double parallel  = vector.dot(vector1);
        // paralleli
        if (abs(parallel) < ERR)
        {
            double s1, s2;
            // Se vengono diversi, il sistema non ha soluzioni e quindi non sono coincidenti
            s1 = (edges[i].start.x - segment.start.x) / x1;
            s2 = (edges[i].start.y - segment.start.y) / y1;

            // paralleli coincidenti
            if (abs( s1-s2) < ERR)
            {
              edges[i].intersection = true;
              edges[i].parallel = true;
              PointInter p(edges[i].start, s1);
              interPoints.push_back(p);
            }
        }
        // incidenti
        else
        {
            Matrix2d A;
            A << xi, -x1,
                 yi, -y1;
            Vector2d b(segment.start.x- edges[i].start.x, segment.start.y- edges[i].start.y);
            Vector2d t = (A.inverse())*b;

            // intersezione interna al lato
            if ( t(0) > +ERR and t(0)< 1-ERR)
            {
                edges[i].intersection = true;
                // verificare che viene lo stesso punto con i due parametri
                edges[i].pointIntersection.x = edges[i].start.x + t(0)*xi;
                edges[i].pointIntersection.y = edges[i].start.y + t(0)*yi;

                PointInter p(edges[i].pointIntersection, t(1));
                interPoints.push_back(p);

            }
            else if(abs(t(0)) < ERR)
            {
                // Se l'intersezione è in un estremo la salvo solo all'inizio per evitare doppioni
                PointInter p(edges[i].start, t(1));
                interPoints.push_back(p);
            }

        }

    }
    return;
}

vector<PointInter> Polygon::ShowInterctions()
{
// viene diverso, in certi casi, a seconda se siano stati già creati set di taglio oppure no perchè vengono
// tolti eventualmente dei punti se ci sono lati paralleli. Alternativa creare una copia delle intersezioni per creare i set di taglio
// ma è memoria aggiuntiva, capire se ne vale la pena
    if(interPoints.size() == 0)
        ComputeIntersection();

    return interPoints;
}

bool Polygon::LeftOfSegment(Point& p)
{
    if(segment.start.x == segment.end.x)
    { // Retta verticale
        return p.x < segment.start.x;
    }
    else
    {
        double yOnSegment = segment.start.y + ((p.x - segment.start.x)/(segment.end.x - segment.start.x))*(segment.end.y - segment.start.y);
        return p.y > yOnSegment;
    }
}

bool Polygon::SameSide(Point &p, Point &q)
{
    if(segment.start.x == segment.end.x)
    { // Retta verticale
        return (p.x < segment.start.x and q.x < segment.start.x) or (p.x > segment.start.x and q.x > segment.start.x);
    }
    else
    {
        double y1OnSegment = segment.start.y + ((p.x - segment.start.x)/(segment.end.x - segment.start.x))*(segment.end.y - segment.start.y);
        double y2OnSegment = segment.start.y + ((q.x - segment.start.x)/(segment.end.x - segment.start.x))*(segment.end.y - segment.start.y);
        return (p.y > y1OnSegment and q.y > y2OnSegment) or (p.y < y1OnSegment and q.y < y2OnSegment);
    }
}

void Polygon::DuplicatePointSet()
{
    int numInter = interPoints.size();
    int numVertices = points.size();
    int i = 0;
    while(i < numInter)
    {
        for (int k=0; k<numVertices; k++)
        {
            if (interPoints[i].p == points[k].p)
            {
                bool add = true;
                for (int s =0; s<numVertices; s++)
                {
                    if (edges[s].parallel and (interPoints[i].p == edges[s].start or interPoints[i].p == edges[s].end))
                    {
                        add = false;
                    }
                    if(edges[s].start == interPoints[i].p and edges[s-1].end == interPoints[i].p)
                    {
                        if (LeftOfSegment(edges[s].end) xor LeftOfSegment(edges[s-1].start))
                        {
                            add = false;
                        }

                    }
                }
                if (add)
                {
                    interPoints.insert(interPoints.begin()+i, interPoints[i]);
                    numInter++;
                    i++;
                    break;
                }
            }
        }
       i++;
    }
}

void Polygon::ParallelCase()
{
    int numParallel = 0;
    int id = -1;
    for (int i=0; i<numEdges; i++)
    {
        if (edges[i].parallel)
        {
            numParallel++;
            id = i;
        }
    }
    if (numParallel > 1)
        throw runtime_error("Segement parallel to multiple edges");
    else if (numParallel ==1)
    {
        PointInter p1= PointInter(), p2 = PointInter();
        int pointBeforep1 = 0;
        int numInter = interPoints.size();
        for (int i=0; i< numInter;i++)
        {
            if (interPoints[i].p != edges[id].start and interPoints[i].p != edges[id].end)
            {
                pointBeforep1++;
            }
            else
            {
                p1 = interPoints[i];
                p2 = interPoints[i+1];
                break;
            }
        }
        int pointAfterp2 = numInter - pointBeforep1 - 2;
        if (pointBeforep1 % 2 ==0)
            interPoints.erase(interPoints.begin()+pointBeforep1);
        if (pointAfterp2 % 2 == 0)
            interPoints.erase(interPoints.end()- pointAfterp2);
    }
}


void Polygon::CreateSet()
{
    // ordina per parametro crescente
    sort(interPoints.begin(),interPoints.end());
    ParallelCase();
    DuplicatePointSet();

  // Creazione dei set aggiungendo eventuali estremi del segmento escluso se appartengono a lati paralleli
  int numberSets = interPoints.size() / 2;
  setTaglio.resize(numberSets);
  PointInter start(segment.start, 0);
  PointInter end(segment.end, 1);
  for (int i = 0; i < numberSets; i++)
  {
      setTaglio[i].push_back(interPoints[2*i]);
      if(start < interPoints[2*i + 1] and start > interPoints[2*i])
          setTaglio[i].push_back(start);
      if(end < interPoints[2*i + 1] and end > interPoints[2*i])
          setTaglio[i].push_back(end);
      setTaglio[i].push_back(interPoints[2*i + 1]);
  }

}

int Polygon::FindSetTaglio(Point& p)
{
  vector<int> index;
  int numSets = setTaglio.size();
  for (int  i = 0; i < numSets; i++)
  {
      int numPoints = setTaglio[i].size();
      for(int k=0; k< numPoints; k++)
      {
          if(setTaglio[i][k].p == p )
          {
              index.push_back(i);
              // Se non è il primo, inverto ordine
              if(setTaglio[i][0].p != p)
                  reverse(setTaglio[i].begin(), setTaglio[i].end());
              break;
          }
      }
  }


  if (index.empty())
  {
    return -1;
  }
  else if (index.size() == 1)
      {
      return index[0];
      }
      else
      //Bisogna scegliere set giusto, p in questo caso è un vertice
      //Passo i lati fino a trovare p come fine di un lato, se l'inizio del lato sta sopra alla retta di taglio ritorno indice maggiore, se no viceversa
        {
         int numEdges = edges.size();
         Point q;
         for ( int i= 0; i< numEdges; i++)
           {
            if (edges[i].end == p)
             {
                q =  edges[i].start;
                break;
             }
           }
         if(LeftOfSegment(q))
         {
             return max(index[0], index[1]);
         }
         else
         {
             return min(index[0], index[1]);
         }
        }
}

int Polygon::FindNextSet(Point& p, int& id, Point& StartPolygon)
{
    int index = -1;
    int numSets = setTaglio.size();
    // Caso in cui inizio lato e inizio poligono sono dallo stesso lato della retta di taglio non deve seguire altri set
    for (int i=0; i< numEdges; i++)
    {
        if (p == edges[i].end)
        {   // Inizio lato e inizio poligono sono dallo stesso lato della retta di taglio

            if (SameSide(StartPolygon,edges[i].start))
                return -1;

        }
    }
    if ((id-1)>= 0)
    {
    int numPoints = setTaglio[id-1].size();
        for (int k= 0; k< numPoints; k++)
        {
            if (p == setTaglio[id-1][k].p)
            {
                index = id-1;
                // Se non è il primo, inverto ordine
                if(setTaglio[id-1][0].p != p)
                    reverse(setTaglio[id-1].begin(), setTaglio[id-1].end());
                break;
            }

        }
    }
    if((id+1) < numSets)
    {
    int numPoints = setTaglio[id+1].size();
        for (int k= 0; k< numPoints; k++)
        {
            if (p == setTaglio[id+1][k].p)
            {
                index = id+1;
                // Se non è il primo, inverto ordine
                if(setTaglio[id+1][0].p != p)
                    reverse(setTaglio[id+1].begin(), setTaglio[id+1].end());
                break;
            }
         }

    }
    return index;
}

int Polygon::FindEdge(Point& p)
{
    int index;

    for (int i=0; i < numEdges; i++)
    {
        if (p == edges[i].start)
        {
            index = i;
        }
        else if ((edges[i].intersection == true) and (p == edges[i].pointIntersection))
        {
            index = i;
        }
    }
    return index;
}

int Polygon::AddSetsPoints(int& id, Polygon &newPolygon, int start, int& i)
{
    int sizeSet = setTaglio[id].size();
    for(int s = start; s<sizeSet; s++)
    {
      Vertex v(setTaglio[id][s].p, -1);
      newPolygon.points.push_back(v);
    }
    // Cerca e percorre eventuali altri set che partono da dove finisce questo
    int newId = FindNextSet(setTaglio[id][sizeSet -1].p, id, newPolygon.points[0].p);
    while ( newId >= 0)
    {
        id = newId;
        sizeSet = setTaglio[newId].size();
        for (int n= 1; n < sizeSet; n++)
        {
            Vertex v(setTaglio[newId][n].p, -1);
            newPolygon.points.push_back(v);
        }
        newId = FindNextSet(setTaglio[newId][sizeSet -1].p, newId, newPolygon.points[0].p);
    }


    int idNewEdge = FindEdge(setTaglio[id][sizeSet -1].p);

    if(edges[idNewEdge].end != edges[i].start)
     {
      Vertex v(edges[idNewEdge].end, -1);
      newPolygon.points.push_back(v);
      reached[idNewEdge + 1] = true;
      return idNewEdge ;
     }
    else
     {
     // In questo non caso non devo andare oltre, se no non esce dal loop, il problema è quando l'intersezione è sull'ultimo lato
       return idNewEdge - 1;
     }
}

void Polygon::CutPolygon()
{
    // Calcolo intersezioni
    if(interPoints.size() == 0)
    ComputeIntersection();

    // Crea set
    if(setTaglio.size() == 0)
    CreateSet();

   int numPoints = points.size();
   // Crea sottopoligoni
   for(int i = 0; i < numPoints; i++)
   {
       // Parte ogni volta da un vertice non ancora raggiunto
       if (reached[i] == false)
       {
           Polygon polygon;
           polygon.points.push_back(points[i]);
           reached[i] = true;

           // Finisce quando torna al punto di partenza
           // '% numEdges' per far riniziare dal primo lato quando finisce loop
         for (int k = i; edges[k % numEdges].end != edges[i].start ; k++)
           {
             int index = k % numEdges;
             int idSet = FindSetTaglio(edges[index].start);
            // idSet è -1 se non c'è nessun set di taglio che parte da quel punto
             if (idSet < 0 and (edges[index].intersection == false))
             {
              Vertex v(edges[index].end, -1);
              polygon.points.push_back(v);
              reached[index + 1] = true;
             }
             else if(idSet >= 0)
             {
                k = AddSetsPoints(idSet, polygon, 1,i);
             }
             else
             {
              idSet = FindSetTaglio(edges[index].pointIntersection);
              k = AddSetsPoints(idSet, polygon, 0,i);
             }
            }
         newPolygons.push_back(polygon);
       }
   }

   // Aggiungere estremi del segmento di taglio se appartengono a un lato parallelo
    AddSegmentPoints();

   // Numera sottopoligoni
    NumSubPolygons();
   return;

}

int Polygon::FindVertexId(Point& p)
{
    int index = -1;
    int size = newPoints.size();
    for (int i = 0; i < size; i++)
    {
        if (p == newPoints[i].p)
            index = newPoints[i].id;
    }
    return index;
}

void Polygon::AddSegmentPoints()
{
    int numSubPolygons = newPolygons.size();
    // Cerco se ci sono lati paralleli al segmento su cui c'è un estremo del segmento
    for ( int i = 0; i< numEdges; i++)
    {
        if (edges[i].parallel == true)
        {
            // trovato il lato parallelo guardo se gli estremi del segmento sono da aggiungere guardando il parametro degli estremi del lato
            bool addStart = false, addEnd = false;
            double tStart = (edges[i].start.x - segment.start.x)/(segment.end.x - segment.start.x);
            double tEnd = (edges[i].end.x - segment.start.x)/(segment.end.x - segment.start.x);
            double t1 = min(tStart, tEnd);
            double t2 = max(tStart, tEnd);
            if (t1< 0 and t2 > 0)
                addStart = true;
            if (t1 < 1 and t2 > 1)
                addEnd = true;

            for (int k = 0; k < numSubPolygons and (addEnd or addStart); k++)
               {
                int numVertices = newPolygons[k].points.size();
                for (int s = 0; s< numVertices; s++)
                {
                    if ( newPolygons[k].points[s].p == edges[i].start and newPolygons[k].points[(s+1)%numVertices].p == edges[i].end)
                    {
                        if ( addStart and addEnd )
                        {
                        // Devo aggiungere prima il punto più vicino all'inizio del lato
                            if (abs(tStart - 0) < abs(tStart - 1))
                            {
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ (s+1),Vertex(segment.start,-1));
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+(s+2),Vertex(segment.end,-1));
                            }
                            else
                            {
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ (s+1),Vertex(segment.end,-1));
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+(s+2),Vertex(segment.start,-1));
                            }
                        }
                        else if(addStart)
                            {
                              newPolygons[k].points.insert(newPolygons[k].points.begin()+ (s+1),Vertex(segment.start,-1));
                            }
                            else if (addEnd)
                             {
                               newPolygons[k].points.insert(newPolygons[k].points.begin()+ (s+1),Vertex(segment.end,-1));
                             }
                        break;
                    }
                    if ( newPolygons[k].points[s].p == edges[i].start and newPolygons[k].points[(s-1)%numVertices].p == edges[i].end)
                    {

                        if ( addStart and addEnd )
                        {
                        // Devo aggiungere prima il punto più vicino all'inizio del lato
                            if (abs(tStart - 0) < abs(tStart - 1))
                            {
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.start,-1));
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.end,-1));
                            }
                            else
                            {
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.end,-1));
                                newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.start,-1));
                            }
                        }
                        else if(addStart)
                            {
                              newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.start,-1));
                            }
                            else if (addEnd)
                             {
                               newPolygons[k].points.insert(newPolygons[k].points.begin()+ s,Vertex(segment.end,-1));
                             }
                        break;
                     }
                 }
                }
          }
       }
}

void Polygon::NumSubPolygons()
{
    int numPolygons = newPolygons.size();
    int newId = points.size();
    for ( int i = 0; i < numPolygons; i++)
    {
        int numPoints = newPolygons[i].points.size();
        for (int k = 0; k< numPoints; k++)
        {
            int id = FindVertexId(newPolygons[i].points[k].p);
            if (id == -1)
            {
                newPolygons[i].points[k].id = newId;
                newPoints.push_back(newPolygons[i].points[k]);
                newId++;
            }
            else
                newPolygons[i].points[k].id = id;
        }
    }
}


vector<Polygon> Polygon::ShowNewPolygons()
{
    if(newPolygons.size() == 0)
        CutPolygon();

    return newPolygons;
}

vector<Vertex> Polygon::ShowNewPoints()
{
    return newPoints;
}



}
