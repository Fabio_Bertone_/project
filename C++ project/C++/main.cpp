#include "test_polygonclass.hpp"

#include <gtest/gtest.h>

#include "Eigen"


int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();


}
