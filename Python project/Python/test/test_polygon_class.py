from unittest import TestCase


import src.PointClass as PointClass
import src.PolygonClass as PolygonClass



class TestPolygonClass (TestCase):
    def testIntersections(self):
        Points = []
        for i in range(3):
            Points.append(i)

        p1 = PointClass.Point(1, 1)
        p2 = PointClass.Point(4, 1)
        p3 = PointClass.Point(4, 4)
        p4 = PointClass.Point(1, 4)
        Vertices = []
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)

        Segment = []
        start = PointClass.Point(2, 2)
        end = PointClass.Point(3, 2)
        Segment.append(start)
        Segment.append(end)
        expectedInter = []
        i1 = PointClass.Point(4, 2)
        i2 = PointClass.Point(1, 2)
        expectedInter.append(i1)
        expectedInter.append(i2)
        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)
        intersections = Polygon.ShowIntersection()
        try:
            for i in range(len(intersections)):
                self.assertEqual(expectedInter[i], intersections[i].p)
        except Exception as ex:
            self.fail()

    def testPolygon1(self):
        Points = []
        for i in range(4):
            Points.append(i)

        Vertices = []
        p0 = PointClass.Point(1.5, 1.0)
        p1 = PointClass.Point(5.6, 1.5)
        p2 = PointClass.Point(5.5, 4.8)
        p3 = PointClass.Point(4.0, 6.2)
        p4 = PointClass.Point(3.2, 4.2)
        p5 = PointClass.Point(1.0, 4.0)
        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)
        Vertices.append(p5)

        Segment = []
        start = PointClass.Point(2.0, 3.7)
        end = PointClass.Point(4.1, 5.9)
        Segment.append(start)
        Segment.append(end)

        expectedInter = []
        i1 = PointClass.Point(4.2043, 6.0093)
        i2 = PointClass.Point(3.7213, 5.5033)
        i3 = PointClass.Point(2.4086, 4.1281)
        i4 = PointClass.Point(1.1912, 2.8527)
        expectedInter.append(i1)
        expectedInter.append(i2)
        expectedInter.append(i3)
        expectedInter.append(i4)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)
        intersections = Polygon.ShowIntersection()
        try:
            for i in range(len(intersections)):
                self.assertEqual(expectedInter[i], intersections[i].p)
        except Exception as ex:
            self.fail()

        Polygons = []
        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(p1, 1))
        Polygon1.append(PointClass.Vertex(p2, 2))
        Polygon1.append(PointClass.Vertex(i1, 6))
        Polygon1.append(PointClass.Vertex(end, 7))
        Polygon1.append(PointClass.Vertex(i2, 8))
        Polygon1.append(PointClass.Vertex(p4, 4))
        Polygon1.append(PointClass.Vertex(i3, 9))
        Polygon1.append(PointClass.Vertex(start, 10))
        Polygon1.append(PointClass.Vertex(i4, 11))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p3, 3))
        Polygon2.append(PointClass.Vertex(i2, 8))
        Polygon2.append(PointClass.Vertex(end, 7))
        Polygon2.append(PointClass.Vertex(i1, 6))

        Polygon3 = []
        Polygon3.append(PointClass.Vertex(p5, 5))
        Polygon3.append(PointClass.Vertex(i4, 11))
        Polygon3.append(PointClass.Vertex(start, 10))
        Polygon3.append(PointClass.Vertex(i3, 9))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)
        Polygons.append(Polygon3)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p , Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id , Polygons[i][k].id)
                except Exception as ex:
                    self.fail()

    def testPolygon2(self):
        Vertices = []
        p0 = PointClass.Point(2, -2)
        p1 = PointClass.Point(0, -1)
        p2 = PointClass.Point(3, 1)
        p3 = PointClass.Point(0, 2)
        p4 = PointClass.Point(3, 2)
        p5 = PointClass.Point(3, 3)
        p6 = PointClass.Point(-1, 3)
        p7 = PointClass.Point(-3, 1)
        p8 = PointClass.Point(0, 0)
        p9 = PointClass.Point(-3, -2)

        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)
        Vertices.append(p5)
        Vertices.append(p6)
        Vertices.append(p7)
        Vertices.append(p8)
        Vertices.append(p9)

        Points = []
        for i in range(9):
            Points.append(i)

        Segment = []
        start = PointClass.Point(-4, -4)
        end = PointClass.Point(4, 4)
        Segment.append(start)
        Segment.append(end)

        i1 = PointClass.Point(1.5, 1.5)
        i2 = PointClass.Point(2, 2)
        i3 = PointClass.Point(-2, -2)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)

        Polygons = []
        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(p1, 1))
        Polygon1.append(PointClass.Vertex(p2, 2))
        Polygon1.append(PointClass.Vertex(i1, 10))
        Polygon1.append(PointClass.Vertex(p8, 8))
        Polygon1.append(PointClass.Vertex(i3, 11))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p3, 3))
        Polygon2.append(PointClass.Vertex(i2, 12))
        Polygon2.append(PointClass.Vertex(p5, 5))
        Polygon2.append(PointClass.Vertex(p6, 6))
        Polygon2.append(PointClass.Vertex(p7, 7))
        Polygon2.append(PointClass.Vertex(p8, 8))
        Polygon2.append(PointClass.Vertex(i1, 10))

        Polygon3 = []
        Polygon3.append(PointClass.Vertex(p4, 4))
        Polygon3.append(PointClass.Vertex(p5, 5))
        Polygon3.append(PointClass.Vertex(i2, 12))

        Polygon4 = []
        Polygon4.append(PointClass.Vertex(p9, 9))
        Polygon4.append(PointClass.Vertex(i3, 11))
        Polygon4.append(PointClass.Vertex(p8, 8))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)
        Polygons.append(Polygon3)
        Polygons.append(Polygon4)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p, Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id, Polygons[i][k].id)
                except Exception as ex:
                    self.fail()

    def testPolygon3(self):
        Vertices = []
        p0 = PointClass.Point(2, -2)
        p1 = PointClass.Point(0, -1)
        p2 = PointClass.Point(3, 1)
        p3 = PointClass.Point(0, 2)
        p4 = PointClass.Point(3, 2)
        p5 = PointClass.Point(3, 3)
        p6 = PointClass.Point(-1, 3)
        p7 = PointClass.Point(-3, 1)
        p8 = PointClass.Point(0, 0)
        p9 = PointClass.Point(-3, -2)

        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)
        Vertices.append(p5)
        Vertices.append(p6)
        Vertices.append(p7)
        Vertices.append(p8)
        Vertices.append(p9)

        Points = []
        for i in range(9):
            Points.append(i)

        Segment = []
        start = PointClass.Point(0, -4)
        end = PointClass.Point(0, 4)
        Segment.append(start)
        Segment.append(end)

        i1 = PointClass.Point(0, -2)
        i2 = PointClass.Point(0, 3)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)

        Polygons = []

        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(p1, 1))
        Polygon1.append(PointClass.Vertex(i1, 10))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p2, 2))
        Polygon2.append(PointClass.Vertex(p3, 3))
        Polygon2.append(PointClass.Vertex(p8, 8))
        Polygon2.append(PointClass.Vertex(p1, 1))

        Polygon3 = []
        Polygon3.append(PointClass.Vertex(p4, 4))
        Polygon3.append(PointClass.Vertex(p5, 5))
        Polygon3.append(PointClass.Vertex(i2, 11))
        Polygon3.append(PointClass.Vertex(p3, 3))

        Polygon4 = []
        Polygon4.append(PointClass.Vertex(p6, 6))
        Polygon4.append(PointClass.Vertex(p7, 7))
        Polygon4.append(PointClass.Vertex(p8, 8))
        Polygon4.append(PointClass.Vertex(p3, 3))
        Polygon4.append(PointClass.Vertex(i2, 11))

        Polygon5 = []
        Polygon5.append(PointClass.Vertex(p9, 9))
        Polygon5.append(PointClass.Vertex(i1, 10))
        Polygon5.append(PointClass.Vertex(p1, 1))
        Polygon5.append(PointClass.Vertex(p8, 8))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)
        Polygons.append(Polygon3)
        Polygons.append(Polygon4)
        Polygons.append(Polygon5)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p, Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id, Polygons[i][k].id)
                except Exception as ex:
                    self.fail()

    def testRectangle(self):
        Vertices = []
        p0 = PointClass.Point(1, 1)
        p1 = PointClass.Point(5, 1)
        p2 = PointClass.Point(5, 3.1)
        p3 = PointClass.Point(1, 3.1)

        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)

        Points = []
        for i in range(3):
            Points.append(i)

        Segment = []
        start = PointClass.Point(2, 1.2)
        end = PointClass.Point(4, 3)
        Segment.append(start)
        Segment.append(end)

        i1 = PointClass.Point(1.7778, 1)
        i2 = PointClass.Point(4.1111, 3.1)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)

        Polygons = []
        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(i1, 4))
        Polygon1.append(PointClass.Vertex(start, 5))
        Polygon1.append(PointClass.Vertex(end, 6))
        Polygon1.append(PointClass.Vertex(i2, 7))
        Polygon1.append(PointClass.Vertex(p3, 3))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p1, 1))
        Polygon2.append(PointClass.Vertex(p2, 2))
        Polygon2.append(PointClass.Vertex(i2, 7))
        Polygon2.append(PointClass.Vertex(end, 6))
        Polygon2.append(PointClass.Vertex(start, 5))
        Polygon2.append(PointClass.Vertex(i1, 4))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p, Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id, Polygons[i][k].id)
                except Exception as ex:
                    self.fail()



    def testPentagon(self):
        Vertices = []
        p0 = PointClass.Point(2.5, 1)
        p1 = PointClass.Point(4, 2.1)
        p2 = PointClass.Point(3.4, 4.2)
        p3 = PointClass.Point(1.6, 4.2)
        p4 = PointClass.Point(1, 2.1)

        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)

        Points = []
        for i in range(4):
            Points.append(i)

        Segment = []
        start = PointClass.Point(1.4, 2.75)
        end = PointClass.Point(3.6, 2.2)
        Segment.append(start)
        Segment.append(end)

        i1 = PointClass.Point(1.2, 2.8)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)

        Polygons = []
        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(p1, 1))
        Polygon1.append(PointClass.Vertex(end, 5))
        Polygon1.append(PointClass.Vertex(start, 6))
        Polygon1.append(PointClass.Vertex(i1, 7))
        Polygon1.append(PointClass.Vertex(p4, 4))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p2, 2))
        Polygon2.append(PointClass.Vertex(p3, 3))
        Polygon2.append(PointClass.Vertex(i1, 7))
        Polygon2.append(PointClass.Vertex(start, 6))
        Polygon2.append(PointClass.Vertex(end, 5))
        Polygon2.append(PointClass.Vertex(p1, 1))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p, Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id, Polygons[i][k].id)
                except Exception as ex:
                    self.fail()

    def testPolygonParallel(self):
        Points = []
        for i in range(4):
            Points.append(i)

        Vertices = []
        p0 = PointClass.Point(1.5, 1.0)
        p1 = PointClass.Point(5.6, 1.5)
        p2 = PointClass.Point(5.5, 4.8)
        p3 = PointClass.Point(4.0, 6.2)
        p4 = PointClass.Point(3.2, 4.2)
        p5 = PointClass.Point(1.0, 4.0)
        Vertices.append(p0)
        Vertices.append(p1)
        Vertices.append(p2)
        Vertices.append(p3)
        Vertices.append(p4)
        Vertices.append(p5)

        Segment = []
        start = PointClass.Point(1.66, 4.06)
        end = PointClass.Point(4.3, 4.3)
        Segment.append(start)
        Segment.append(end)

        i1 = PointClass.Point(5.5118, 4.4102)

        Polygon = PolygonClass.Polygon(Vertices, Points, Segment)

        Polygons = []
        Polygon1 = []
        Polygon1.append(PointClass.Vertex(p0, 0))
        Polygon1.append(PointClass.Vertex(p1, 1))
        Polygon1.append(PointClass.Vertex(i1, 6))
        Polygon1.append(PointClass.Vertex(end, 7))
        Polygon1.append(PointClass.Vertex(p4, 4))
        Polygon1.append(PointClass.Vertex(start, 8))
        Polygon1.append(PointClass.Vertex(p5, 5))

        Polygon2 = []
        Polygon2.append(PointClass.Vertex(p2, 2))
        Polygon2.append(PointClass.Vertex(p3, 3))
        Polygon2.append(PointClass.Vertex(p4, 4))
        Polygon2.append(PointClass.Vertex(end, 7))
        Polygon2.append(PointClass.Vertex(i1, 6))

        Polygons.append(Polygon1)
        Polygons.append(Polygon2)

        newPolygons = Polygon.ShowNewPolygon()
        for i in range(len(Polygons)):
            for k in range(len(Polygons[i])):
                try:
                    self.assertEqual(newPolygons[i].GetVertex(k).p, Polygons[i][k].p)
                    self.assertEqual(newPolygons[i].GetVertex(k).id, Polygons[i][k].id)
                except Exception as ex:
                    self.fail()