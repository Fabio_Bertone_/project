from src.PointClass import *
import src.SegmentClass as SegmentClass
import numpy as np
import math as math

err = 10E-4


class Polygon:
    def __init__(self, Vertices=None, PolygonVertices=None, Segment=None):
        if Vertices is None:
            Vertices = []
        if PolygonVertices is None:
            PolygonVertices = []
        if Segment is None:
            Segment = []
        self.__Vertices = []
        self.__reached = []
        self.__edges = []
        self.__PointInter = []
        self.__SetTaglio = []
        self.__newPolygons = []
        self.__newPoints = []

        for i in range(0, len(Vertices)):
            self.__Vertices.append(Vertex(Vertices[i], i))
            self.__newPoints.append(Vertex(Vertices[i], i))
            s = SegmentClass.Segment(Vertices[i], Vertices[(i + 1) % len(Vertices)])
            self.__edges.append(s)
            self.__reached.append(False)
        self.__numEdges = len(self.__edges)
        if len(Segment) > 0:
            if Segment[0].x < Segment[1].x:
                self.__Segment = SegmentClass.Segment(Segment[0], Segment[1])
            elif Segment[0].x != Segment[1].x:
                self.__Segment = SegmentClass.Segment(Segment[1], Segment[0])
            else:
                p1 = Point(Segment[0].x, min(Segment[0].y, Segment[1].y))
                p2 = Point(Segment[0].x, max(Segment[0].y, Segment[1].y))
                self.__Segment = SegmentClass.Segment(p1, p2)

    def NumEdges(self) -> int:
        return self.__numEdges


    def AddVertex(self, p: Vertex):
        self.__Vertices.append(p)
        self.__newPoints.append(p)
        self.__reached.append(False)
        if len(self.__Vertices) > 1:
            # elimino precedente chiusura del poligono
            if len(self.__edges) != 0:
                self.__edges.pop(len(self.__Vertices) - 2)
            # nuovo lato
            s1 = SegmentClass.Segment(self.__Vertices[len(self.__Vertices) - 2].p, p.p)
            # Lato che chiude il poligono
            s2 = SegmentClass.Segment(p.p, self.__Vertices[0].p)
            self.__edges.append(s1)
            self.__edges.append(s2)
            if self.__numEdges == 0:
                self.__numEdges = self.__numEdges + 2
            else:
                self.__numEdges = self.__numEdges + 1

    # Aggiunge un vertice all'interno del poligono e non in coda
    def AddInteriorVertex(self, p: Vertex, pos: int):
        self.__edges.pop(pos)
        self.__Vertices.insert(pos +1 , p)
        self.__edges.insert(pos, SegmentClass.Segment(self.__Vertices[pos].p,p.p))
        self.__edges.insert(pos + 1, SegmentClass.Segment(p.p, self.__Vertices[(pos + 1)%self.__numEdges].p))
        self.__numEdges += 1

    def ClearPolygon(self):
        self.__PointInter.clear()
        self.__SetTaglio.clear()
        self.__newPolygons.clear()
        for i in range(self.__numEdges):
            self.__reached[i] = False
            self.__edges[i].intersection = False
            self.__edges[i].parallel = False
        for k in range(len(self.__Vertices), len(self.__newPoints)):
            self.__newPoints.pop(k)

    def ChangeSegment(self, p1: Point, p2: Point):
        if p1.x < p2.x:
            self.__Segment = SegmentClass.Segment(p1, p2)
        elif p1.x != p2.x:
            self.__Segment = SegmentClass.Segment(p2, p1)
        else:
            t1 = Point(p1.x, min(p1.y, p2.y))
            t2 = Point(p1.x, max(p1.y, p2.y))
            self.__Segment = SegmentClass.Segment(t1, t2)
        self.ClearPolygon()


    def GetVertex(self, pos) -> Vertex:
        return self.__Vertices[pos]

    def __ComputeIntersection(self):
        x1 = self.__Segment.end.x - self.__Segment.start.x
        y1 = self.__Segment.end.y - self.__Segment.start.y
        vector1 = np.array([x1, y1])
        for edge in self.__edges:
            xi = edge.end.x - edge.start.x
            yi = edge.end.y - edge.start.y
            # vettore [xi, yi] ruotato di 90° in senso antiorario
            vector2 = np.array([-yi, xi])
            if abs(np.dot(vector1, vector2)) < err:
                # x = xs + x1*t
                # y = ys + y1*t
                # xe = xs + x1*t
                # ye = ys + y1*t
                # Verifica se il punto iniziale del lato appartiene al segmento
                s1 = 0
                s2 = 0
                if x1 != 0:
                    s1 = (edge.start.x -self.__Segment.start.x) / x1
                else:
                    s1 = math.inf
                if y1 != 0:
                    s2 = (edge.start.y -self.__Segment.start.y) / y1
                else:
                    s2 = math.inf

                if abs(s1 - s2) < err:
                    edge.intersection = True
                    edge.parallel = True
                    self.__PointInter.append(PointInter(edge.start, s1))
            else:
                # x = xs + x1*t
                # y = ys + y1*t
                # x = xe + xi*s
                # y = ye + yi*s
                # At = b
                A = np.array([[xi, -x1], [yi, -y1]])
                b = np.array([self.__Segment.start.x - edge.start.x, self.__Segment.start.y - edge.start.y])
                t = np.linalg.solve(A, b)

                if err < t[0] < 1 - err:
                    edge.intersection = True
                    edge.pointIntersection.x = edge.start.x + t[0] * xi
                    edge.pointIntersection.y = edge.start.y + t[0] * yi

                    self.__PointInter.append(PointInter(edge.pointIntersection, t[1]))

                elif abs(t[0]) < err:
                    self.__PointInter.append(PointInter(edge.start, t[1]))

    def ShowIntersection(self) -> list:
        if (len(self.__PointInter) == 0):
            self.__ComputeIntersection()

        return self.__PointInter

    def __LeftOfSegment(self, p: Point) -> bool:
        if self.__Segment.start.x == self.__Segment.end.x:
            return p.x < self.__Segment.start.x
        else:
            #y - y0 = m*(x - x0)
            m = (self.__Segment.end.y - self.__Segment.start.y)/ (self.__Segment.end.x - self.__Segment.start.x)
            yOnSegment = self.__Segment.start.y + ((p.x - self.__Segment.start.x) * m)
            return p.y > yOnSegment

    def __SameSide(self, p: Point, q: Point) -> bool:
        if self.__Segment.start.x == self.__Segment.end.x:
            #retta verticale
            xSegment = self.__Segment.start.x
            return (p.x < xSegment and q.x < xSegment) or (p.x > xSegment and q.x > xSegment)
        else:
            #y - y0 = m*(x - x0)
            m = (self.__Segment.end.y - self.__Segment.start.y)/ (self.__Segment.end.x - self.__Segment.start.x)
            y1OnSegment = self.__Segment.start.y + ((p.x - self.__Segment.start.x) * m)
            y2OnSegment = self.__Segment.start.y + ((q.x - self.__Segment.start.x) * m)
            return (p.y > y1OnSegment and q.y > y2OnSegment) or (p.y < y1OnSegment and q.y < y2OnSegment)

    # Duplica punti necessari per creare i set nel caso in cui ci sia intersezione interna e un vertice serve in più set
    def __DuplicatePointSet(self):
        numInter = len(self.__PointInter)
        numVertices = len(self.__Vertices)
        i = 0
        while i < numInter:
            for k in range(numVertices):
                if self.__PointInter[i].p == self.__Vertices[k].p:
                    add = True
                    for s in range(numVertices):
                        if self.__edges[s].parallel == True and (
                                self.__PointInter[i].p == self.__edges[s].start or self.__PointInter[i].p ==
                                self.__edges[s].end):
                            add = False
                        if self.__edges[s].start == self.__PointInter[i].p and self.__edges[s - 1].end == \
                                self.__PointInter[i].p:
                            # XOR
                            if self.__LeftOfSegment(self.__edges[s].end) ^ self.__LeftOfSegment(
                                    self.__edges[s - 1].start):
                                add = False
                    if add:
                        self.__PointInter.insert(i, self.__PointInter[i])
                        numInter += 1
                        i += 1
                        break
            i += 1

    def __ParallelCase(self):
        numParallel = 0
        id = -1
        for i in range(self.__numEdges):
            if self.__edges[i].parallel == True:
                numParallel += 1
                id = i
        if numParallel > 1:
            raise Exception("Segment parallel to multiple edges")
        ## P1 è il primo punto sulla retta di taglio, P2 il secondo. Conto i punti prima di p1 e dopo p2 e nel caso siano
        ## pari vanno tolti perché non servono per creare set di taglio
        elif numParallel == 1:
            p1, p2 = 0, 0
            pointBeforep1 = 0
            for i in range(len(self.__PointInter)):
                if self.__PointInter[i].p != self.__edges[id].start and self.__PointInter[i].p != self.__edges[id].end:
                    pointBeforep1 += 1
                else:
                    p1 = self.__PointInter[i]
                    p2 = self.__PointInter[i + 1]
                    break
            # Punti dopo p2 sono i punti totali meno i punti prima di p1 e tolti ancora p1 e p2
            pointAfterp2 = len(self.__PointInter) - pointBeforep1 - 2
            if (pointBeforep1 % 2) == 0:
                self.__PointInter.remove(p1)
            if (pointAfterp2 % 2) == 0:
                self.__PointInter.remove(p2)

    def __CreateSet(self):
        self.__PointInter.sort()
        self.__ParallelCase()
        self.__DuplicatePointSet()

        numberSets = int(len(self.__PointInter) / 2)
        start = PointInter(self.__Segment.start, 0)
        end = PointInter(self.__Segment.end, 1)
        for i in range(numberSets):
            set = []
            set.append(self.__PointInter[2 * i])
            if self.__PointInter[2 * i] < start < self.__PointInter[2 * i + 1]:
                set.append(start)
            if self.__PointInter[2 * i] < end < self.__PointInter[2 * i + 1]:
                set.append(end)
            set.append(self.__PointInter[2 * i + 1])
            self.__SetTaglio.append(set)

    def __FindSetTaglio(self, p: Point) -> int:
        index = []
        numSets = len(self.__SetTaglio)
        for i in range(numSets):
            numPoints = len(self.__SetTaglio[i])
            for k in range(numPoints):
                if self.__SetTaglio[i][k].p == p:
                    index.append(i)
                    if self.__SetTaglio[i][0].p != p:
                        self.__SetTaglio[i].reverse()
                    break
        if len(index) == 0:
            return -1
        elif len(index) == 1:
            return index[0]
        else:
            for i in range(self.__numEdges):
                if self.__edges[i].end == p:
                    q = self.__edges[i].start
                    if self.__LeftOfSegment(q):
                        return max(index[0], index[1])
                    else:
                        return min(index[0], index[1])

    def __FindNextSet(self, p: Point, id: int, StartPolygon: Point) -> int:
        index = -1
        numSets = len(self.__SetTaglio)
        for i in range(self.__numEdges):
            if (p == self.__edges[i].end) and ( self.__SameSide(StartPolygon,self.__edges[i].start)):
                return -1

        if id - 1 >= 0:
            numPoints = len(self.__SetTaglio[id - 1])
            for k in range(numPoints):
                if p == self.__SetTaglio[id - 1][k].p:
                    index = id - 1
                    if self.__SetTaglio[id - 1][0].p != p:
                        self.__SetTaglio[id - 1].reverse()
                    break

        if id + 1 < numSets:
            numPoints = len(self.__SetTaglio[id + 1])
            for k in range(numPoints):
                if p == self.__SetTaglio[id + 1][k].p:
                    index = id + 1
                    if self.__SetTaglio[id + 1][0].p != p:
                        self.__SetTaglio[id + 1].reverse()
                    break

        return index

    def __FindEdge(self, p: Point) -> int:
        numEdges = len(self.__edges)
        for i in range(numEdges):
            if p == self.__edges[i].start:
                return i
            elif self.__edges[i].intersection == True and p == self.__edges[i].pointIntersection:
                return i

    def __FindVertexId(self, p: Point) -> int:
        index = -1
        size = len(self.__newPoints)
        for i in range(size):
            if p == self.__newPoints[i].p:
                index = self.__newPoints[i].id
        return index

    def __NumSubPolygons(self):
        numPolygons = len(self.__newPolygons)
        newId = len(self.__Vertices)
        for i in range(numPolygons):
            numPoints = len(self.__newPolygons[i].__Vertices)
            for k in range(numPoints):
                id = self.__FindVertexId(self.__newPolygons[i].__Vertices[k].p)
                if id == -1:
                    self.__newPolygons[i].__Vertices[k].id = newId
                    self.__newPoints.append(self.__newPolygons[i].__Vertices[k])
                    newId += 1
                else:
                    self.__newPolygons[i].__Vertices[k].id = id

    def __AddSetsPoints(self, id: int, newPolygon, start: int, i: int) -> int:
        sizeSet = len(self.__SetTaglio[id])
        for s in range(start, sizeSet):
            v = Vertex(self.__SetTaglio[id][s].p, -1)
            newPolygon.AddVertex(v)
        newId = self.__FindNextSet(self.__SetTaglio[id][sizeSet - 1].p, id, newPolygon.GetVertex(0).p)
        while newId >= 0:
            id = newId
            sizeSet = len(self.__SetTaglio[newId])
            for n in range(1, sizeSet):
                v = Vertex(self.__SetTaglio[newId][n].p, -1)
                newPolygon.AddVertex(v)
            newId = self.__FindNextSet(self.__SetTaglio[newId][sizeSet - 1].p, newId, newPolygon.GetVertex(0).p)
        idNewEdge = self.__FindEdge(self.__SetTaglio[id][sizeSet - 1].p)
        if self.__edges[idNewEdge].end != self.__edges[i].start:
            v = Vertex(self.__edges[idNewEdge].end, -1)
            newPolygon.AddVertex(v)
            self.__reached[idNewEdge + 1] = True
            k = idNewEdge
        else:
            k = idNewEdge - 1
        return k

    def __AddSegmentPoints(self):
        addStart, addEnd = False, False
        p1, p2 = Point(), Point()
        for i in range(self.__numEdges):
            if self.__edges[i].parallel:
                # verifico se gli estremi del segmento sono da aggiugengere guardando se appartiene al lato
                p1 = self.__edges[i].start
                p2 = self.__edges[i].end
                tStart = (self.__edges[i].start.x - self.__Segment.start.x) / (
                            self.__Segment.end.x - self.__Segment.start.x)
                tEnd = (self.__edges[i].end.x - self.__Segment.start.x) / (
                            self.__Segment.end.x - self.__Segment.start.x)
                t1 = min(tStart, tEnd)
                t2 = max(tStart, tEnd)
                if t1 < 0 < t2:
                    addStart = True
                if t1 < 1 < t2:
                    addEnd = True
        for k in range(len(self.__newPolygons)):
            numVertices = self.__newPolygons[k].__numEdges
            for i in range(numVertices):
                # Se trovo il lato parallelo al segmento
                if ((self.__newPolygons[k].GetVertex(i).p == p1 and self.__newPolygons[k].GetVertex(
                        (i + 1) % numVertices).p == p2) or
                        (self.__newPolygons[k].GetVertex(i).p == p2 and self.__newPolygons[k].GetVertex(
                            (i + 1) % numVertices).p == p1)):
                    # se sono da aggiungere entrambi devo verificare l'ordine
                    if addStart and addEnd:
                        tStart = (self.__newPolygons[k].GetVertex(i).p.x - self.__Segment.start.x) / (
                                self.__Segment.end.x - self.__Segment.start.x)
                        # se l'inizio del segmento è più vicino all'inizio del lato
                        if abs(tStart - 0) < abs(tStart - 1):
                            self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.start, -1), i)
                            self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.end,-1), i + 1)
                        else:
                            self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.end, -1), i)
                            self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.start,-1), i + 1)
                    elif addStart:
                        self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.start,-1), i)
                    elif addEnd:
                        self.__newPolygons[k].AddInteriorVertex(Vertex(self.__Segment.end,-1 ), i)

    def __CutPolygon(self):
        if len(self.__PointInter) == 0:
            self.__ComputeIntersection()
        if len(self.__SetTaglio) == 0:
            self.__CreateSet()
        numPoints = len(self.__Vertices)

        for i in range(numPoints):
            if not self.__reached[i]:
                newPolygon = Polygon()
                newPolygon.AddVertex(self.__Vertices[i])
                self.__reached[i] = True

                k = i
                while self.__edges[k % self.__numEdges].end != self.__edges[i].start:
                    index = k % self.__numEdges
                    idSet = self.__FindSetTaglio(self.__edges[index].start)
                    if idSet < 0 and (self.__edges[index].intersection == False):
                        v = Vertex(self.__edges[index].end, -1)
                        newPolygon.AddVertex(v)
                        self.__reached[index + 1] = True
                    elif idSet >= 0:
                        k = self.__AddSetsPoints(idSet, newPolygon, 1, i)
                    else:
                        idSet = self.__FindSetTaglio(self.__edges[index].pointIntersection)
                        k = self.__AddSetsPoints(idSet, newPolygon, 0, i)
                    k += 1
                self.__newPolygons.append(newPolygon)
        self.__AddSegmentPoints()
        self.__NumSubPolygons()

    def ShowNewPolygon(self) -> list:
        if len(self.__newPolygons) == 0:
            self.__CutPolygon()
        return self.__newPolygons

    def ShowNewPoints(self) -> list:
        if len(self.__newPolygons) == 0:
            self.__CutPolygon()
        return self.__newPoints
