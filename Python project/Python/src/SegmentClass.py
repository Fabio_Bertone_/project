from src.PointClass import *


class Segment:
    def __init__(self, start:Point, end:Point):
        self.start = start
        self.end = end
        self.intersection = False
        self.parallel = False
        self.pointIntersection = Point()