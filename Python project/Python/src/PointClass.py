err = 10E-4
class Point :
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return(abs(self.x - other.x) < err and abs(self.y - other.y) < err)

    def  __ne__(self, other):
        return (abs(self.x - other.x) > err or abs(self.y - other.y) > err)

class PointInter(Point):
    def __init__(self, p : Point, t):
        self.p = p
        self.t = t

    def __lt__(self, other):
        return(self.t < other.t)

    def __gt__(self, other):
        return(self.t > other.t)

class Vertex(Point):
    def __init__(self, p:Point, id):
        self.p = p
        self.id = id


