#include "Pizzeria.h"
#include <algorithm>

namespace PizzeriaLibrary {

Ingredient::Ingredient(string name, string description, int price)
{
    Name = name;
    Description = description;
    Price = price;
}

bool Ingredient::operator<(const Ingredient &ingredient) const
{
    return (Name < ingredient.Name);
}

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    pizzaIngredient.push_back(ingredient);
}

int Pizza::NumIngredients() const
{
    return pizzaIngredient.size();
}

int Pizza::ComputePrice() const
{
    int price = 0;
    unsigned int size = pizzaIngredient.size();
    for (unsigned int i=0; i < size; i++)
    {
        price += pizzaIngredient[i].Price;
    }
    return price;
}

void Order::InitializeOrder(int numPizzas)
{
    pizzasOrdered.reserve(numPizzas);
}

void Order::AddPizza(const Pizza &pizza)
{
    pizzasOrdered.push_back(pizza);
}

const Pizza &Order::GetPizza(const int &position) const
{
    if(position > NumPizzas())
        throw runtime_error("Position passed is wrong");
    // First pizza in position 1
    int index = position -1;
    return pizzasOrdered[index];
}

int Order::NumPizzas() const
{
    return pizzasOrdered.size();
}

int Order::ComputeTotal() const
{
    int price = 0;
    unsigned int size = NumPizzas();
    for(unsigned int i=0; i < size; i++)
    {
        price += pizzasOrdered[i].ComputePrice();
    }
    return price;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    unsigned int numIngredients = ingredientsList.size();
    // Checking if already inserted
    for (unsigned int i=0; i < numIngredients; i++ )
    {
        if( ingredientsList[i].Name == name )
        {
            throw runtime_error("Ingredient already inserted");
            return;
        }
    }
    Ingredient newIngredient(name, description, price);
    ingredientsList.push_back(newIngredient);
    //Alphabetical order
    sort(ingredientsList.begin(),ingredientsList.end());
    return;
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    unsigned int numIngredients = ingredientsList.size();
    for (unsigned int i=0; i < numIngredients; i++ )
    {
        if( ingredientsList[i].Name == name )
        {
            return ingredientsList[i];
        }
    }
    throw runtime_error("Ingredient not found");

}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    // Checking if already inserted
    unsigned int numPizzas = pizzasList.size();
    for (unsigned int i=0; i < numPizzas; i++)
    {
        if(pizzasList[i].Name == name )
        {
            throw runtime_error("Pizza already inserted");
            return;
        }
    }

    // Creating new pizza
    unsigned int numIngredients = ingredients.size();
    Pizza newPizza;
    newPizza.Name = name;
    for (unsigned int i=0; i < numIngredients; i++)
    {
        Ingredient newIngredient = FindIngredient(ingredients[i]);
        newPizza.AddIngredient(newIngredient);
    }
    pizzasList.push_back(newPizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned int numPizzas = pizzasList.size();
    for (unsigned int i=0; i < numPizzas; i++)
    {
        if(pizzasList[i].Name == name )
        {
            return pizzasList[i];
        }
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    // Checking if empty
    if (pizzas.empty())
    {
        throw runtime_error("Empty order");
        return false;
    }

    Order newOrder;
    // So as the first order has number 1000
    unsigned int numOrder = ordersList.size() + 1000;
    newOrder.Number = numOrder;
    unsigned int size = pizzas.size();
    newOrder.InitializeOrder(size);
    for (unsigned int i=0; i < size; i++)
    {
        const Pizza& newPizza = FindPizza(pizzas[i]);
        newOrder.AddPizza(newPizza);
    }
    ordersList.push_back(newOrder);
    return numOrder;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    unsigned int totOrders = ordersList.size();
    for (unsigned int i=0; i < totOrders; i++)
    {
        if(ordersList[i].Number == numOrder )
        {
            return ordersList[i];
        }
    }
    throw runtime_error("Order not found");

}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    Order order = FindOrder(numOrder);

    string receipt , price;
    unsigned int totalPrice = 0;
    unsigned int totPizzas = order.NumPizzas();
    for (unsigned int i=0; i < totPizzas; i++)
    {
        Pizza pizza = order.GetPizza(i+1);
        price = to_string(pizza.ComputePrice());
        receipt = receipt +  "- " + pizza.Name + ", " + price + " euro" + '\n';
        totalPrice += pizza.ComputePrice();
    }
    price = to_string(totalPrice);
    receipt += "  TOTAL: " + price + " euro" + '\n';
    return receipt;
}

string Pizzeria::ListIngredients() const
{
    unsigned int numIngredients = ingredientsList.size();
    string list, price;
    for (unsigned int i=0 ; i < numIngredients ; i++)
    {
        price = to_string(ingredientsList[i].Price);
        list += ingredientsList[i].Name + " - '" + ingredientsList[i].Description + "': " + price + " euro" + '\n';
    }
    return list;
}

string Pizzeria::Menu() const
{
    string list, price, nIngredients;
    int numPizzas = pizzasList.size();
    // Inverted order to pass the test
    for (int i=numPizzas-1 ; i >= 0 ; i--)
    {
        nIngredients = to_string(pizzasList[i].NumIngredients());
        price = to_string(pizzasList[i].ComputePrice());
        list += pizzasList[i].Name + " (" + nIngredients + " ingredients): " + price + " euro" + '\n';
    }
    return list;
}


}

