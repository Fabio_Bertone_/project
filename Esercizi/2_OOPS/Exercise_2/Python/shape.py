import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.__center = center
        self.__a = a
        self.__b = b

    def area(self) -> float:
        return self.__a * self.__b * math.pi

class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        self.__center = center
        self.__radius = radius

    def area(self) -> float:
        return   math.pi * self.__radius **2

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3

    def area(self) -> float:
        l1 = math.sqrt((self.__p1.x - self.__p2.x) **2 + (self.__p1.y - self.__p2.y) **2 )
        l2 = math.sqrt((self.__p2.x - self.__p3.x) **2 + (self.__p2.y - self.__p3.y) **2 )
        l3 = math.sqrt((self.__p1.x - self.__p3.x) ** 2 + (self.__p1.y - self.__p3.y) ** 2)
        # semiperimeter
        p = (l1 + l2 + l3) / 2
        # Heron's formula
        return math.sqrt(p * (p-l1) * (p-l2) * (p-l3))

class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__p1 = p1
        self.__edge = edge

    def area(self) -> float:
        return self.__edge **2 * math.sqrt(3) / 4

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
        self.__p4 = p4

    def area(self) -> float:
        l1 = math.sqrt((self.__p1.x - self.__p2.x) ** 2 + (self.__p1.y - self.__p2.y) ** 2)
        l2 = math.sqrt((self.__p2.x - self.__p3.x) ** 2 + (self.__p2.y - self.__p3.y) ** 2)
        l3 = math.sqrt((self.__p3.x - self.__p4.x) ** 2 + (self.__p3.y - self.__p4.y) ** 2)
        l4 = math.sqrt((self.__p4.x - self.__p1.x) ** 2 + (self.__p4.y - self.__p1.y) ** 2)
        diag = math.sqrt((self.__p4.x - self.__p2.x) ** 2 + (self.__p4.y - self.__p2.y) ** 2)

        # quadrilateral as sum of two triangles using Heron's formula
        p = (diag + l1 + l4) / 2
        area1 = math.sqrt(p * (p-diag) * (p - l1) * (p - l4))
        p = (diag + l2 + l3) / 2
        area2 = math.sqrt(p * (p - diag) * (p - l2) * (p - l3))
        return area1 + area2

class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p4 = p4

    def area(self) -> float:
        base = math.sqrt((self.__p1.x - self.__p2.x) **2 + (self.__p1.y - self.__p2.y) **2 )
        # straight line passing from p1 and p2
        m = (self.__p2.y - self.__p1.y) / (self.__p2.x - self.__p1.x)
        q = self.__p1.y - m * self.__p1.x
        # distance between p4 and line
        height = math.fabs(self.__p4.y - (m * self.__p4.x + q)) / math.sqrt(1 + m*m)
        return base * height

class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.__p1 = p1
        self.__base = base
        self.__height = height

    def area(self) -> float:
        return self.__base * self.__height

class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.__p1 = p1
        self.__edge = edge

    def area(self) -> float:
        return self.__edge ** 2