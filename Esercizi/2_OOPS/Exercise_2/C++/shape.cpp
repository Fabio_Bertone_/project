#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

Point::Point(const Point &point)
{
    _x = point._x;
    _y = point._y;
}


Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Area() const
{
    return _a*_b*M_PI;
}

Circle::Circle(const Point &center, const int &radius)
{
    _center = center;
    _radius = radius;
}

double Circle::Area() const
{
    return M_PI*_radius*_radius;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
}

double Triangle::Area() const
{
    double l1, l2, l3;
    l1 = sqrt((_p1._x-_p2._x)*(_p1._x-_p2._x)+(_p1._y-_p2._y)*(_p1._y-_p2._y));
    l2 = sqrt((_p2._x-_p3._x)*(_p2._x-_p3._x)+(_p2._y-_p3._y)*(_p2._y-_p3._y));
    l3 = sqrt((_p1._x-_p3._x)*(_p1._x-_p3._x)+(_p1._y-_p3._y)*(_p1._y-_p3._y));

    // semiperimeter
    double p;
    p = (l1+ l2+ l3)/2;

    // Heron's formula
    return sqrt(p*(p-l1)*(p-l2)*(p-l3));
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    _p1 = p1;
    _edge = edge;
}

double TriangleEquilateral::Area() const
{
    return _edge*_edge*sqrt(3)/4;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
}

double Quadrilateral::Area() const
{
    double l1, l2, l3, l4, diag;
    l1 = sqrt((_p1._x-_p2._x)*(_p1._x-_p2._x)+(_p1._y-_p2._y)*(_p1._y-_p2._y));
    l2 = sqrt((_p2._x-_p3._x)*(_p2._x-_p3._x)+(_p2._y-_p3._y)*(_p2._y-_p3._y));
    l3 = sqrt((_p4._x-_p3._x)*(_p4._x-_p3._x)+(_p4._y-_p3._y)*(_p4._y-_p3._y));
    l4 = sqrt((_p4._x-_p1._x)*(_p4._x-_p1._x)+(_p4._y-_p1._y)*(_p4._y-_p1._y));
    diag = sqrt((_p4._x-_p2._x)*(_p4._x-_p2._x)+(_p4._y-_p2._y)*(_p4._y-_p2._y));

    // quadrilateral as sum of two triangles
    double area1, area2, p;
    p = (diag + l1 + l4) / 2;
    area1 = sqrt(p * (p - diag) * (p - l1) * (p - l4));
    p = (diag + l2 + l3) / 2;
    area2 = sqrt(p * (p - diag) * (p - l2) * (p - l3));
    return area1 + area2;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    _p1 = p1;
    _p2 = p2;
    _p4 = p4;
}

double Parallelogram::Area() const
{
    double base, height, m, q;
    base = sqrt((_p1._x-_p2._x)*(_p1._x-_p2._x)+(_p1._y-_p2._y)*(_p1._y-_p2._y));
    m = (_p2._y-_p1._y)/(_p2._x-_p1._x);
    q = _p1._y - m*_p1._x;
    height = fabs((_p4._y)-(m*_p4._x + q))/sqrt(1 + m*m);
    return base * height;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    _p1 = p1;
    _base = base;
    _height = height;
}

double Rectangle::Area() const
{
    return _base * _height;
}

Square::Square(const Point &p1, const int &edge)
{
    _p1 = p1;
    _edge = edge;
}

double Square::Area() const
{
    return _edge * _edge;
}

}
