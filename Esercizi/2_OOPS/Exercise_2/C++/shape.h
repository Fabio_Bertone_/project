#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double _x;
      double _y;
      Point(const double& x,
            const double& y);
      // Empty constructor
      Point() {_x=0; _y=0;}
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    private:
    Point _center;
    int _a=0;
    int _b;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public IPolygon
  {
    private:
    Point _center;
    int _radius;
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
    Point _p1, _p2, _p3;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };


  class TriangleEquilateral : public IPolygon
  {
    private:
    Point _p1;
    int _edge;
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    Point _p1, _p2,_p3, _p4;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const ;
  };


  class Parallelogram : public IPolygon
  {
    Point _p1, _p2, _p4;
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public IPolygon
  {
    Point _p1;
    int _base;
    int _height;
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public IPolygon
  {
    Point _p1;
    int _edge;
    public:
      Square(const Point& p1,
             const int& edge);
      double Area() const;
  };
}

#endif // SHAPE_H
