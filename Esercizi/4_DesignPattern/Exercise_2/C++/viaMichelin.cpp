# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
  // Reset
  _numberBuses = 0;
  _buses.clear();

  // Open File
  ifstream file;
  file.open(_busFilePath.c_str());
  if(file.fail())
      throw runtime_error("Something goes wrong");

  // Load
  try {
  string line;
  getline(file, line); // skip comment
  getline(file, line);
  istringstream converterN;
  converterN.str(line);
  converterN >> _numberBuses;

  getline(file, line); // skip comment
  getline(file, line); // skip comment
  _buses.resize(_numberBuses);
  for(int i=0; i < _numberBuses; i++ )
    {
      getline(file, line);
      istringstream converter;
      converter.str(line);
      converter >> _buses[i].Id >> _buses[i].FuelCost;
    }

  // close file
  file.close();
  }
  catch (exception)
  {
      // Reset
      _numberBuses = 0;
      _buses.clear();

      throw runtime_error("something goes wrong");
  }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus > _numberBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus-1];

}

void MapData::reset()
{
    _numberBusStops = 0;
    _numberRoutes = 0;
    _numberStreets = 0;
    _busStops.clear();
    _routes.clear();
    _streets.clear();
    _streetFrom.clear();
    _streetTo.clear();
    _routeStreets.clear();
}

void MapData::Load()
{
    // Reset
    reset();

    // Open File
    ifstream file;
    file.open(_mapFilePath.c_str());

    if(file.fail())
        throw runtime_error("Something goes wrong");

    // Load
    try
    {
    string line;
    // Bus Stops
    getline(file, line); //Skip comment line
    getline(file, line);
    istringstream converterBusStops;
    converterBusStops.str(line);
    converterBusStops >> _numberBusStops;

    getline(file, line); //Skip comment line
    _busStops.resize(_numberBusStops);
    for (int i=0; i<_numberBusStops; i++)
      {
       getline(file, line);
       istringstream converter;
       converter.str(line);
       converter >> _busStops[i].Id >> _busStops[i].Name >> _busStops[i].Latitude >> _busStops[i].Longitude;
      }
    // Streets
    getline(file, line); //Skip comment line
    getline(file, line);
    istringstream converterStreets;
    converterStreets.str(line);
    converterStreets >> _numberStreets;

    getline(file, line); //Skip comment line
    _streets.resize(_numberStreets);
    _streetFrom.resize(_numberStreets);
    _streetTo.resize(_numberStreets);
    for (int k=0; k<_numberStreets; k++)
      {
      getline(file,line);
      istringstream converter;
      converter.str(line);
      converter >> _streets[k].Id >> _streetFrom[k] >> _streetTo[k] >> _streets[k].TravelTime;
      }

    // Routes
    getline(file, line); //Skip comment line
    getline(file, line);
    istringstream converterRoutes;
    converterRoutes.str(line);
    converterRoutes >> _numberRoutes;

    getline(file, line); //Skip comment line
    _routes.resize(_numberRoutes);
    _routeStreets.resize(_numberRoutes);
    for (int s=0; s<_numberRoutes; s++)
      {
      getline(file,line);
      istringstream converter;
      converter.str(line);
      converter >> _routes[s].Id >> _routes[s].NumberStreets;
      _routeStreets[s].resize(_routes[s].NumberStreets);
      for (int j=0; j<_routes[s].NumberStreets; j++)
        {
          converter >> _routeStreets[s][j];
        }
      }
    // File close
    file.close();
    }
    catch (exception)
    {
    reset();
    throw runtime_error ("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if(idRoute > _numberRoutes)
        throw runtime_error("Route "+ to_string(idRoute)+ " does not exists");
    if (streetPosition > (_routes[idRoute-1].NumberStreets - 1))
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

   int idStreet = _routeStreets[idRoute - 1][streetPosition];
   return _streets[idStreet -1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if(idRoute > _numberRoutes)
        throw runtime_error("Route "+ to_string(idRoute)+ " does not exists");
    return _routes[idRoute -1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if (idStreet > _numberStreets)
        throw runtime_error("Street "+ to_string(idStreet)+ " does not exists");
    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if (idStreet > _numberStreets)
        throw runtime_error("Street "+ to_string(idStreet)+ " does not exists");

    int idBusStop = _streetFrom[idStreet -1];
    return _busStops[idBusStop -1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if (idStreet > _numberStreets)
        throw runtime_error("Street "+ to_string(idStreet)+ " does not exists");

    int idBusStop = _streetTo[idStreet -1];
    return _busStops[idBusStop -1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if (idBusStop > _numberBusStops)
        throw runtime_error ("BusStop "+ to_string(idBusStop)+ " does not exists");

    return _busStops[idBusStop -1];
}

    // In RoutePlanner and MapViewer check on id are not necessary because are included in _mapData and _busStation
int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    int totalTravelTime = 0;
    const Route& route = _mapData.GetRoute(idRoute);
    int numberStreets = route.NumberStreets;
    for (int i=0; i < numberStreets; i++)
      {
        const Street& street = _mapData.GetRouteStreet(idRoute, i);
        totalTravelTime += street.TravelTime;
      }
    return totalTravelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    int totalCost = 0;
    double travelTime = ComputeRouteTravelTime(idRoute);
    // Converting time to hour
    travelTime = travelTime / 3600.0;
    const Bus& bus = _busStation.GetBus(idBus);
    totalCost = travelTime * bus.FuelCost * BusAverageSpeed;
    return totalCost;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    string line = to_string(idRoute) + ": ";
    int numberStreets = _mapData.GetRoute(idRoute).NumberStreets;
    for (int i = 0; i<numberStreets; i++)
     {
     Street street = _mapData.GetRouteStreet(idRoute, i);
     if (i== 0)
         line += _mapData.GetStreetFrom(street.Id).Name; // Avoid first "->"
     else
         line += " -> " + _mapData.GetStreetFrom(street.Id).Name;
     if (i == numberStreets - 1 )
         line += " -> " + _mapData.GetStreetTo(street.Id).Name; // Last stop
     }
    return line;

}

string MapViewer::ViewStreet(const int &idStreet) const
{ 
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    // '/ 10000' to get the right format
    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
