#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}

double Point::ComputeNorm2() const
{
    return sqrt((X*X)+(Y*Y));
}

Point Point::operator+(const Point& point) const
{
  return Point(X + point.X, Y + point.Y);
}

Point Point::operator-(const Point& point) const
{
  return Point(X - point.X, Y - point.Y);
}

Point&Point::operator-=(const Point& point)
{
  X = X - point.X;
  Y = Y - point.Y;
  return *this;
}

Point&Point::operator+=(const Point& point)
{
    X = X + point.X;
    Y = Y + point.Y;
    return *this;
}

Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Perimeter() const
  {
    return 2 * M_PI * sqrt((_a*_a + _b*_b)/2);
  }

Circle::Circle(const Point &center, const double &radius)
{
    _center = center;
    _a = radius;
    _b = radius;
}

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
    points.resize(3);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
  }

  void Triangle::AddVertex(const Point &point)
  {
      points.push_back(point);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    unsigned int numPoints = points.size();
    for (unsigned int i = 0; i < numPoints; i++)
    {
        perimeter += (points[(i+1)%numPoints]-points[i]).ComputeNorm2();
    }

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    // finding other points of a equilateral triangle
    Point p2(p1.X + edge, p1.Y);
    double x3, y3;
    x3 = (p1.X + p2.X) / 2.0;
    y3 = p1.Y + sqrt(edge*edge - (x3-p1.X)*(x3-p1.X));
    Point p3(x3, y3);

    points.resize(3);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
    points.resize(4);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
    points[3] = p4;
  }

  void Quadrilateral::AddVertex(const Point &p)
  {
      points.push_back(p);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    unsigned int numPoints = points.size();
    for (unsigned int i = 0; i < numPoints; i++)
    {
        perimeter += (points[(i+1)%numPoints]-points[i]).ComputeNorm2();
    }

    return perimeter;
  }

  Rectangle::Rectangle(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
    // Finding other points
    Point p2(p1.X + base, p1.Y);
    Point p3(p1.X + base, p1.Y + height);
    Point p4(p1.X, p1.Y + height);

    points.resize(4);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
    points[3] = p4;
  }
  Square::Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

  Square::Square(const Point &p1, const double &edge)
  {
      // Finding other points
      Point p2(p1.X + edge, p1.Y);
      Point p3(p1.X + edge, p1.Y + edge);
      Point p4(p1.X, p1.Y + edge);

      points.resize(4);
      points[0] = p1;
      points[1] = p2;
      points[2] = p3;
      points[3] = p4;
  }

}
