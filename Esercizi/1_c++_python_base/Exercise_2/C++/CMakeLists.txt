cmake_minimum_required(VERSION 3.5)

project(encryption LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(encryption main.cpp)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/text.txt
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
